package com.example.paymentapp.constants;

public final class AppConstants {
	
	private AppConstants() {
		
	}

	public static final String SUCCESS = "SUCCESS";
	
	public static final String USER_ACCOUNT = "USER";
	
	public static final String VENDOR_ACCOUNT = "VENDOR";

	public static final String DEBIT = "DEBIT";
	
	public static final String CREDIT = "CREDIT";

}
