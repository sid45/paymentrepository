package com.example.paymentapp.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.paymentapp.dto.PaymentRequestDto;
import com.example.paymentapp.dto.PaymentResponseDto;
import com.example.paymentapp.exception.InsufficientBalanceException;
import com.example.paymentapp.exception.InvalidUserAccountNumberException;
import com.example.paymentapp.exception.InvalidVendorAccountNumberException;
import com.example.paymentapp.service.PaymentService;
import com.example.paymentapp.service.PaymentServiceImpl;

import io.swagger.annotations.ApiOperation;

/**
 * This is controller class used for making payment. 
 * @author sidramesh.mudhol
 *
 */
@RestController
@RequestMapping("/payment")
public class PaymentAppController {
	
	@Autowired
	private PaymentService paymentService;
	
	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(PaymentAppController.class);
	
	/**
	 * 
	 * @param paymentRequestDto
	 * @return ResponseEntity<PaymentResponseDto>
	 * @throws InvalidUserAccountNumberException, when invalid user account number is given.
	 * @throws InvalidVendorAccountNumberException, when invalidVendorAccountNumber is given.
	 * @throws InsufficientBalanceException , when transaction amount is greater than account balance.
	 */
	@PostMapping("")
	@ApiOperation("payment service")
	public ResponseEntity<PaymentResponseDto> makePayment(@Valid @RequestBody PaymentRequestDto paymentRequestDto) throws InvalidUserAccountNumberException, InvalidVendorAccountNumberException, InsufficientBalanceException {
		logger.info("inside makepayment method");
		PaymentResponseDto paymentResponseDto = paymentService.makePayment(paymentRequestDto);
		return new ResponseEntity<>(paymentResponseDto, HttpStatus.OK);
		
	}

}
