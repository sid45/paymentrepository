package com.example.paymentapp.dto;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentRequestDto {
	
	@NotNull(message = "userAccountNumber is mandatory")
	private String userAccountNumber;
	@NotNull(message = "vendorAccountNumber is mandatory")
	private String vendorAccountNumber;
	
	private double transactionAmount;
	

}
