package com.example.paymentapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaymentResponseDto {

	private String statusMessage;
	
	private int statusCode;
}
