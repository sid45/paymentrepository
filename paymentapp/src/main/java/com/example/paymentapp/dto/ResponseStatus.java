package com.example.paymentapp.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseStatus {
	
	String message;
	int statuscode;

}
