package com.example.paymentapp.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="account")
public class Account {
	
	@Id
	private long accountId;
	
	private String accountNumber;
	
	private double balance;
	
	private LocalDate accountCreatedDate;
	
	private String accountType;
	
	

}
