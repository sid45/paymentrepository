package com.example.paymentapp.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="transaction_detail")
public class TransactionDetail {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long transactionId;
	
	private String AccountNumber;
	
	private double transactionAmount;
	
	private LocalDateTime transactionDate;
	
	private String transactionType;
	
	private String transactionNumber;
	
	private double balance;
	
	
	
	
	
	

}
