package com.example.paymentapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.paymentapp.dto.ResponseStatus;



@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(InvalidUserAccountNumberException.class)
	public ResponseEntity<ResponseStatus> handleInvalidPaymentModeException(InvalidUserAccountNumberException details) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(details.getLocalizedMessage());
		errorStatus.setStatuscode(900);
		return new ResponseEntity<>(errorStatus, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(InvalidVendorAccountNumberException.class)
	public ResponseEntity<ResponseStatus> handleSeatNotFoundException(InvalidVendorAccountNumberException details) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(details.getLocalizedMessage());
		errorStatus.setStatuscode(901);
		return new ResponseEntity<>(errorStatus, HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException ex) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(ex.getBindingResult().getFieldError().getDefaultMessage());
		errorStatus.setStatuscode(904);
		return new ResponseEntity<Object>(errorStatus, HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> handleValidationExceptions1(HttpMessageNotReadableException ex) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(ex.getLocalizedMessage());
		errorStatus.setStatuscode(905);
		return new ResponseEntity<Object>(errorStatus, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InsufficientBalanceException.class)
	public ResponseEntity<Object> handleInsufficientBalanceException1(InsufficientBalanceException ex) {
		ResponseStatus errorStatus = new ResponseStatus();
		errorStatus.setMessage(ex.getLocalizedMessage());
		errorStatus.setStatuscode(905);
		return new ResponseEntity<Object>(errorStatus, HttpStatus.BAD_REQUEST);
	}

}
