package com.example.paymentapp.exception;

public class InvalidUserAccountNumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public InvalidUserAccountNumberException(String message) {
		super(message);
	}

}
