package com.example.paymentapp.exception;

public class InvalidVendorAccountNumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidVendorAccountNumberException(String message) {
		super(message);
	}

}
