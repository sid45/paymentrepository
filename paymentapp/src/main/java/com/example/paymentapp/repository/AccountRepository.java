package com.example.paymentapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.paymentapp.entity.Account;


@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

	Account findByAccountNumber(String userAccountNumber);

}
