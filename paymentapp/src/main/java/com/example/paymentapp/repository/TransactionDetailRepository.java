package com.example.paymentapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.paymentapp.entity.TransactionDetail;

@Repository
public interface TransactionDetailRepository extends CrudRepository<TransactionDetail, Long>{

}
