package com.example.paymentapp.service;

import com.example.paymentapp.dto.PaymentRequestDto;
import com.example.paymentapp.dto.PaymentResponseDto;
import com.example.paymentapp.exception.InsufficientBalanceException;
import com.example.paymentapp.exception.InvalidUserAccountNumberException;
import com.example.paymentapp.exception.InvalidVendorAccountNumberException;

/**
 * This is service class used to make payment.
 * @author sidramesh.mudhol
 *
 */
public interface PaymentService {
	
	/**
	 * 
	 * @param paymentRequestDto
	 * @return PaymentResponseDto
	 * @throws InvalidUserAccountNumberException, when invaliduserAccountnumber is given.
	 * @throws InvalidVendorAccountNumberException, when invalidVendorAccountNumber is given.
	 * @throws InsufficientBalanceException , when transaction amount is greater than account balance. 
	 */
	public PaymentResponseDto makePayment(PaymentRequestDto paymentRequestDto) throws InvalidUserAccountNumberException, InvalidVendorAccountNumberException, InsufficientBalanceException;

}
