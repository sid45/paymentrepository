package com.example.paymentapp.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.paymentapp.constants.AppConstants;
import com.example.paymentapp.dto.PaymentRequestDto;
import com.example.paymentapp.dto.PaymentResponseDto;
import com.example.paymentapp.entity.Account;
import com.example.paymentapp.entity.TransactionDetail;
import com.example.paymentapp.exception.InsufficientBalanceException;
import com.example.paymentapp.exception.InvalidUserAccountNumberException;
import com.example.paymentapp.exception.InvalidVendorAccountNumberException;
import com.example.paymentapp.repository.AccountRepository;
import com.example.paymentapp.repository.TransactionDetailRepository;

/**
 * This is service implementation class used to make payment.
 * @author sidramesh.mudhol
 *
 */
@Transactional
@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private TransactionDetailRepository transactionDetailRepository;

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

	@Override
	public PaymentResponseDto makePayment(PaymentRequestDto paymentRequestDto)
			throws InvalidUserAccountNumberException, InvalidVendorAccountNumberException, InsufficientBalanceException {
		logger.info("inside make payment method");
		PaymentResponseDto paymentResponseDto = new PaymentResponseDto();
		Account userAccount = accountRepository.findByAccountNumber(paymentRequestDto.getUserAccountNumber());
		if (userAccount == null) {
			logger.error("error in make payment method while fetching user account number");
			throw new InvalidUserAccountNumberException("invalid user account number");
		}
		if (userAccount.getBalance() < paymentRequestDto.getTransactionAmount())
			throw new InsufficientBalanceException("insufficient balance");

		Account vendorAccount = accountRepository.findByAccountNumber(paymentRequestDto.getVendorAccountNumber());
		if (vendorAccount == null) {
			logger.error("error in make payment method while fetching vendor account number");
			throw new InvalidVendorAccountNumberException("invalid vendor account number");
		}
		String transactionNumber = RandomStringUtils.randomNumeric(5);
		String transactionMessageForUserAccount = saveTransactionDetails(userAccount.getAccountNumber(),
				userAccount.getAccountType(), userAccount.getBalance(), paymentRequestDto.getTransactionAmount(),
				transactionNumber);
		String transactionMessageForVendorAccount = saveTransactionDetails(vendorAccount.getAccountNumber(),
				vendorAccount.getAccountType(), vendorAccount.getBalance(), paymentRequestDto.getTransactionAmount(),
				transactionNumber);
		if (transactionMessageForUserAccount.equalsIgnoreCase(AppConstants.SUCCESS)
				&& transactionMessageForVendorAccount.equalsIgnoreCase(AppConstants.SUCCESS)) {
			paymentResponseDto.setStatusCode(800);
			paymentResponseDto.setStatusMessage("payment success");
		}

		userAccount.setBalance(userAccount.getBalance() - paymentRequestDto.getTransactionAmount());
		vendorAccount.setBalance(vendorAccount.getBalance() + paymentRequestDto.getTransactionAmount());
		List<Account> accountList = new ArrayList<>();
		accountList.add(userAccount);
		accountList.add(vendorAccount);
		accountRepository.saveAll(accountList);
		logger.info("exiting make payment method");
		return paymentResponseDto;

	}

	/**
	 * 
	 * @param accountNumber
	 * @param accountType
	 * @param balance
	 * @param transactionAmount
	 * @param transactionNumber
	 * @return
	 */
	private String saveTransactionDetails(String accountNumber, String accountType, double balance,
			double transactionAmount, String transactionNumber) {
		logger.info("inside saveTransactionDetails method");
		TransactionDetail transactionDetail = new TransactionDetail();
		transactionDetail.setAccountNumber(accountNumber);
		if (AppConstants.USER_ACCOUNT.equalsIgnoreCase(accountType)) {
			transactionDetail.setBalance(balance - transactionAmount);
			transactionDetail.setTransactionType(AppConstants.DEBIT);
		} else {
			transactionDetail.setBalance(balance + transactionAmount);
			transactionDetail.setTransactionType(AppConstants.CREDIT);
		}
		transactionDetail.setTransactionAmount(transactionAmount);
		transactionDetail.setTransactionDate(LocalDateTime.now());
		transactionDetail.setTransactionNumber(transactionNumber);
		transactionDetailRepository.save(transactionDetail);
		logger.info("exiting saveTransactionDetails method");
		return AppConstants.SUCCESS;

	}

}
