package com.example.paymentapp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.paymentapp.dto.PaymentRequestDto;
import com.example.paymentapp.dto.PaymentResponseDto;
import com.example.paymentapp.exception.InsufficientBalanceException;
import com.example.paymentapp.exception.InvalidUserAccountNumberException;
import com.example.paymentapp.exception.InvalidVendorAccountNumberException;
import com.example.paymentapp.service.PaymentService;

@SpringBootTest
class PaymentAppControllerTest {

	@Mock
	private PaymentService paymentService;
	
	@InjectMocks
	PaymentAppController paymentAppController;
	
	@Test
	public void testMakePayment() throws InvalidUserAccountNumberException, InvalidVendorAccountNumberException, InsufficientBalanceException {
		PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
		PaymentResponseDto responseDto = new PaymentResponseDto();
		responseDto.setStatusCode(900);
		Mockito.when(paymentService.makePayment(paymentRequestDto)).thenReturn(responseDto );
		ResponseEntity<PaymentResponseDto> response = paymentAppController.makePayment(paymentRequestDto);
		Assertions.assertEquals(900, response.getBody().getStatusCode());

	}

}
