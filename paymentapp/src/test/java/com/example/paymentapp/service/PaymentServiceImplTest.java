package com.example.paymentapp.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.paymentapp.dto.PaymentRequestDto;
import com.example.paymentapp.dto.PaymentResponseDto;
import com.example.paymentapp.entity.Account;
import com.example.paymentapp.exception.InsufficientBalanceException;
import com.example.paymentapp.exception.InvalidUserAccountNumberException;
import com.example.paymentapp.exception.InvalidVendorAccountNumberException;
import com.example.paymentapp.repository.AccountRepository;
import com.example.paymentapp.repository.TransactionDetailRepository;

@SpringBootTest
class PaymentServiceImplTest {

	
	@Mock
	private TransactionDetailRepository transactionDetailRepository;
	
	@Mock
	private AccountRepository accountRepository;
	
	@InjectMocks
	PaymentServiceImpl paymentServiceImpl;
	
	PaymentRequestDto paymentRequestDto = new PaymentRequestDto();
	Account userAccount = new Account();
	Account vendorAccount = new Account();
	
	
	@BeforeEach
	public void setUp() {
		paymentRequestDto.setUserAccountNumber("12345");
		paymentRequestDto.setVendorAccountNumber("98765");
		userAccount.setAccountNumber("12345");
		userAccount.setAccountType("USER");
		userAccount.setBalance(5000);
		vendorAccount.setAccountNumber("98765");
		vendorAccount.setAccountType("VENDOR");
		vendorAccount.setBalance(2000);
	}
	
	@Test
	public void testMakePayment() throws InvalidUserAccountNumberException, InvalidVendorAccountNumberException, InsufficientBalanceException {
		Mockito.when(accountRepository.findByAccountNumber(paymentRequestDto.getUserAccountNumber())).thenReturn(userAccount );
		Mockito.when(accountRepository.findByAccountNumber(paymentRequestDto.getVendorAccountNumber())).thenReturn(vendorAccount  );
		PaymentResponseDto response = paymentServiceImpl.makePayment(paymentRequestDto);
		Assertions.assertEquals(800, response.getStatusCode());

	}
	
	@Test
	public void testMakePaymentForInvalidUserAccountNoException() {
		Mockito.when(accountRepository.findByAccountNumber(paymentRequestDto.getUserAccountNumber())).thenReturn(null );
		Assertions.assertThrows(InvalidUserAccountNumberException.class, () -> paymentServiceImpl.makePayment(paymentRequestDto));
	}
	
	
	
	@Test
	public void testMakePaymentForInvalidVendorAccountNoException() {
		
		Mockito.when(accountRepository.findByAccountNumber(paymentRequestDto.getUserAccountNumber())).thenReturn(userAccount );
		Mockito.when(accountRepository.findByAccountNumber(paymentRequestDto.getVendorAccountNumber())).thenReturn(null);
		Assertions.assertThrows(InvalidVendorAccountNumberException.class, () -> paymentServiceImpl.makePayment(paymentRequestDto));
	}
	
}
